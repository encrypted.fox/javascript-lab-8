import {Component} from '@angular/core';
import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';

interface MyUser {
  name?: string;
  surname?: string;
  emails?: string[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  searchUser: string = '';
  changeCheckbox: boolean = false;
  userForm: FormGroup;
  users: MyUser[] = [];
  index: number;

  constructor() {
    this.userForm = new FormGroup({ // создание новой формы
      name: new FormControl(null, [Validators.required]), // поля формы
      surname: new FormControl(null, [Validators.required]),
      emails: new FormArray([ // может быть несколько email, Используем массив
        new FormControl(null, [Validators.required, Validators.email])
      ])
    });
  }

  onUserFormSubmit() {

    if (this.changeCheckbox) {
      this.users[this.index] = this.userForm.value;
      this.changeCheckbox = false;
    } else {

      this.users.push(this.userForm.value); // данные из формы при сабмите добавляем в массив пользователей
    }
    this.userForm.reset(); // сброс значений полей формы

  }

  onAddEmail() {
    (<FormArray>this.userForm.controls['emails']).push(new FormControl(null, [Validators.required, Validators.email]));
    // приводим поле формы к типу FormsArray и добавляем
  }

  onDelEmail(index) {
    (<FormArray>this.userForm.controls['emails']).removeAt(index); // приводим поле формы к типу FormsArray и удаляем элемент по интедксу
  }

  onWorkerChange(user) {
    let index = this.getUserIndex(user);
    this.userForm = new FormGroup({
      name: new FormControl(user.name, [Validators.required]), // поля формы
      surname: new FormControl(user.surname, [Validators.required]),
      emails: new FormArray([])
    });
    for (let i = 0; i < user.emails.length; i++) {
      (<FormArray>this.userForm.controls['emails']).push(new FormControl(user.emails[i], [Validators.email, Validators.required]));
    }
    this.changeCheckbox = true;
    console.log(this.changeCheckbox);
    this.index = index;
  }

  getUserIndex(user) {
    for (let a = 0; a < this.users.length; a++) {
      if (this.users[a] == user) {
        return a;
      }
    }
  }
}
