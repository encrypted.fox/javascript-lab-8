import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilter implements PipeTransform {

  transform(arr, searchStr: string) {
    if (arr.lenght === 0 || searchStr === '') {
      return arr;
    }
    return arr.filter(function (search) {
      if (
        (search.name.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1) ||
        (search.surname.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1)) {
        return true;
      }
      for (let i = 0; i < search.emails.length; i++) {
        if (search.emails[i].toLowerCase().indexOf(searchStr.toLowerCase()) !== -1) {
          return true;
        }
      }
    });

  }
}
